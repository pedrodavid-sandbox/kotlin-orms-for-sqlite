import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import net.harawata.appdirs.AppDirs
import net.harawata.appdirs.AppDirsFactory
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.sqlite.SQLiteException
import java.io.File
import java.sql.DriverManager
import me.pedrodavid.Database as DelightDatabase
import org.jetbrains.exposed.sql.Database as ExposedDatabase


fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
//    connect("test.db")
    val appDirs: AppDirs = AppDirsFactory.getInstance()
    val userDataDir = appDirs.getUserDataDir("sqlite-test", "0.1.0", "pedro.david")
    val databaseName = "test.db"
    val url = "jdbc:sqlite:$userDataDir${File.separatorChar}$databaseName"
    File(userDataDir).mkdirs()
    DriverManager.getConnection(url).use {
        println("Connection to SQLite has been established.")

        it.createStatement().execute(
            """
            CREATE TABLE IF NOT EXISTS expenses (
              id integer PRIMARY KEY,
              name text NOT NULL,
              price double NOT NULL
            );
        """.trimIndent()
        )
    }
}

object Expenses : IntIdTable() {
    val name = text("name")
    val price = double("price")
}

class Expense(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Expense>(Expenses)

    var name by Expenses.name
    var price by Expenses.price
}

fun connect(databaseName: String) {
    val url = "jdbc:sqlite:$databaseName"
    DriverManager.getConnection(url).use {
        println("Connection to SQLite has been established.")

        it.createStatement().execute("""
            CREATE TABLE IF NOT EXISTS expenses (
              id integer PRIMARY KEY,
              name text NOT NULL,
              price double NOT NULL
            );
        """.trimIndent())

        try {
            it.createStatement().executeUpdate("""
                INSERT INTO expenses VALUES (1, 'test', 2.3);
            """.trimIndent())
        } catch (e: SQLiteException) {
            println("Failed to insert expense: \n$e")
        }
    }

    ExposedDatabase.connect("jdbc:sqlite:$databaseName")
    transaction {
        SchemaUtils.create(Expenses)

        try {
            val id = Expenses.insert {
                it[id] = 3
                it[name] = "test 2"
                it[price] = 2.3
            } get Expenses.id
            println("Just create expense with id: $id")
        } catch (e: ExposedSQLException) {
            println("Failed to insert expense: \n$e")
        }

        val expense = Expense.new {
            name = "test 3"
            price = 2.3
        }
        println("Created expense with id '${expense.id}'")

        val single = Expenses.select {
            Expenses.id eq expense.id
        }.single()
        println(single)
    }

    val driver: SqlDriver = JdbcSqliteDriver(url)
    val delightDatabase = DelightDatabase(driver)
    DelightDatabase.Schema.create(driver)
    println(delightDatabase.userQueries.userForName("admin").executeAsOne())
}